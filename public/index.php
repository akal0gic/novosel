<?php

define('BEGIN_AT', microtime(1));
error_reporting(E_ALL);
define('YII_DEBUG', (bool) getenv('YII_DEBUG')); #!
ini_set('display_errors', 1);
setlocale(LC_ALL, 'ru_RU.UTF8');

/** Основные директории приложения
 * @property mixed $projectRoot - корневая директория проекта
 * @property mixed $projectAppRoot - директория приложений
 * @property mixed $projectCache - корневая директория кеша всех приложений
 * @property mixed $projectCommon - директория общих моделей для всех приложений
 * @property mixed $appRoot - директория активного приложени
 * @property mixed $vendors - директория общих расширений в папке с yii */
class ProjectPathes
{
  public $projectRoot;
  public $projectAppRoot;
  public $projectCache;
  public $projectCommon;
  public $appRoot;
  public $appCache;
  public $vendors;

}

/** Основной класс инициализирующий доступные приложения.
 * @property mixed $appName - Название активного приложения
 * @property mixed $appEnv - Среда приложения может принимать значения (LOCAL, DEBUG, PRODUCTION)
 * @property mixed $appConfig - Конфиг приложения */
class Project
{

  protected $serverName;
  protected $path;
  protected $appName;
  protected $appEnv;
  protected $appConfig;

  function __construct()
  {
    $serverName = filter_input(INPUT_SERVER, 'SERVER_NAME');
    $this->serverName = isset($this->appAliasList[$serverName]) ? $this->appAliasList[$serverName] : $serverName;

    $this->path = new ProjectPathes();
    $this->appName = $this->getAppName();
    $this->appEnv = $this->getAppEnv();
    $this->setAppPathes();
    $this->initYii();
    $this->appConfig = $this->loadConfig();
  }

  /** Выбор активного приложения на основе имени домена */
  private function getAppName()
  {
    $this->returnHelper('novosel', 'APP_NAME');
  }

  /** Выбор переменных среды на основе доменного постфикса */
  private function getAppEnv()
  {
    return $this->returnHelper('LOCAL', 'APP_ENV');
  }

  /** Устанавливает основные директории для проекта и активного приложения */
  private function setAppPathes()
  {
    $this->path->projectRoot = realpath(__DIR__ . '/../') . '/';
    $this->path->projectAppRoot = $this->path->projectRoot . 'app/';
    $this->path->projectCache = $this->path->projectRoot . 'cache/';

    $this->path->projectCommon = $this->path->projectAppRoot . '_common/';
    $this->path->appRoot = $this->path->projectAppRoot . $this->appName . '/';
    $this->path->appCache = $this->path->projectCache . $this->appName . '/';

    $this->path->vendors = $this->path->projectRoot . 'vendor/';
  }

  /** Инициализация фреймворка */
  private function initYii()
  {
    # Вызывает фреймворк, подцепляет сахар
    require_once ($this->path->projectRoot . 'vendor/yii1/yii.php');
    require_once ($this->path->projectRoot . 'vendor/yii1/library/global.php');

    # Устанавливает основные директории для проекта и активного приложения как алиасы пути для Yii
    foreach ($this->path as $alias => $path) {
      Yii::setPathOfAlias($alias, $path);
    }
    Yii::setPathOfAlias('bootstrap', $this->path->projectRoot . 'vendor/yii1/vendors/yiibooster/');
  }

  /** Загружает конфиг с параматрами окружения. */
  private function loadConfig()
  {
    # определяет необходимость пересобрать конфиг
    $configFileName = $this->path->projectCache . 'config/' . $this->appName . '.' . $this->appEnv . '.php';
    $configFileExist = file_exists($configFileName);

    $env = require($this->path->appRoot . 'config/env/' . $this->appEnv . '.php');
    $config = require($this->path->appRoot . 'config/main.php');

    return $config;
  }

  /** Запускает фреймворк */
  function run()
  {
    $yii = Yii::createApplication('CWebApplication', $this->appConfig);
    $yii->run();
  }

  /** Проверяет существует ли скалярное значение у $value. И объявляет константу если это необходимо.
   * @param type $value - Любое скалярное значение.
   * @param mixed|null $name - Если TRUE объявляет (define) константу с именем $name
   * @return mixed  - возвращет значение $value
   * @throws Exception - Если $value - NULL	 */
  private function returnHelper($value, $name = NULL)
  {
    if (is_null($value)) throw new Exception('Only scalar values are allowed.');
    if (!is_null($name) AND ! defined($name)) define($name, $value);

    return $value;
  }

}

$project = new Project();
$project->run();
