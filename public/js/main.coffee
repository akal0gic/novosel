orders = {
    # парсит JSON в теле HTML
    json_data: (id, key = false) ->
        data = JSON.parse $("##{id}").html()
        if key then data[key] else data

    # хелпер поиска элемента
    _get_goods_line_cost: (elem) ->
    	wrapper = $(elem).closest '.js-order-form-goods-line'
    	cost_label = $(wrapper).find '.js-order-form-goods-line-cost'

    # при смене товара в выпадающем списке изменить цену и пересчитать итоговую сумму
    goods_changed: (event) ->
        console.log 1312
        
        goods = @.json_data 'js-list-of-available-goods', event.val

        cost_label = @._get_goods_line_cost event.target
        cost_label.attr 'data-cost', goods.cost
        cost_label.html goods.cost

        @.goods_total_cost()

    # при смене кол-ва товаров пересчитать итоговую стоимость всех товаров
    goods_count_changed: (elem) ->
    	cost_label = @._get_goods_line_cost elem
    	cost_label.attr 'data-count', $(elem).val()

    	@.goods_total_cost()

    # пересчитать итоговую стоимость всех товаров
    goods_total_cost: () ->
    	total = 0;
    	$('.js-order-form-goods-line-cost').each ->
    		total += $(@).attr('data-cost') * $(@).attr('data-count')
    		
    	$('.js-order-form-goods-total-cost').html total
}



$(document).ready ->

    # добавить новый набор товаров к заказу   
    $(document).on 'click', '#js-order-form-goods-add', ->
        $.ajax {
            'url': '/order/_form_goods/', async: true,
            success: (response) ->
                $('.js-order-form-goods-lines').append response
                $('select').select2('width':'resolve').on 'change', (e) ->
                    orders.goods_changed e
                0;

        }

    # хук на изменения кол-ва товаров в списке
    $(document).on 'change', 'input[id ^=OrderGoods_][id $=_count]', () -> 
        orders.goods_count_changed(this)