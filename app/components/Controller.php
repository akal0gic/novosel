<?php

/**
 * Базовый расширенный контроллер 
 */
class Controller extends CController
{

  public $layout = '//layouts/layout_main';

  /** статичные страницы */
  public function actions()
  {
    return ['page' => ['class' => 'CViewAction']];
  }

  /** Создает новый экземпляр модели или заргужает существующий
   * @param int|null  - PK
   * @param int|null  - имя модели, по умолчанию берет имя из $this->model_name
   * @throws CHttpException
   * @return ActiveRecord
   */
  public function initModel($id = null, $modelName = null)
  {
    # имя модели
    if (is_null($modelName) && !($modelName = $this->modelName))
        throw new CHttpException(404, 'Запрошенная страница не существует.');

    # экземпляр модели
    $model = ($id) ? $model = $modelName::model()->findByPk($id) : (new $modelName);
    if (!$model) throw new CHttpException(404, 'Запрошенная страница не существует.');

    return $model;
  }

}
