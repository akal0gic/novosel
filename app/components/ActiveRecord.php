<?php

/** Расширенный класс of \CActiveRecord
 * Дополнен фильтрами, правилами, сахаром и т.д.
 */
class ActiveRecord extends CActiveRecord
{

  /** @return mixed Default primary key */
  public function primaryKey()
  {
    return 'id';
  }

  /** Стандартные правила и фильтры валидации телефонного номера */
  public function defaultRulesAndFiltersForPhone($attribute = 'phone', $errorMsg = 'Используйте верный телефонный формат, например - "8 (903) 123-45-67"')
  {
    return [
        __METHOD__ . '1' => [$attribute, 'match', 'pattern' => '/^[0-9|\(|\)|\-\+| ]*$/', 'message' => $errorMsg, 'on' => 'insert'],
        __METHOD__ . '2' => [$attribute, 'filter', 'filter' => [$this, 'filterDoDigitsOnly']],
        __METHOD__ . '3' => [$attribute, 'filter', 'filter' => [$this, 'filterDoFormatLocalPhoneNumber']],
        __METHOD__ . '4' => [$attribute, 'match', 'pattern' => '/^[0-9].{9,9}$/', 'message' => $errorMsg, 'on' => 'insert'],
    ];
  }

  /** Фильтр валидации убирает пробелы в скалярных значениях переданных атрибутов. Если не передан список атрибутов, убирет у всех */
  public function filterTrim($attributes = NULL)
  {
    if (is_null($attributes)) $attributes = implode(', ', array_keys($this->getAttributes()));
    return [
        __METHOD__ . '1' => [$attributes, 'filter', 'filter' => [$this, 'filterDoTrim']],
    ];
  }

  /** Фильтр валидации отсекающий любые символы кроме цифр */
  public function filterDoDigitsOnly($value)
  {
    return preg_replace('#[\D]*#', '', $value);
  }

  /** Фильтр валидации телефонного номера отсекающий "8" и "+7" перед телефонным номером */
  public function filterDoFormatLocalPhoneNumber($value)
  {
    return preg_replace('#^89#', '9', preg_replace('#^79#', '9', $value));
  }

  /** убирает пробелы в скалярных величинах */
  public function filterDoTrim($value)
  {
    return (is_scalar($value) ? trim($value) : $value);
  }

}
