<?php

return [
    'basePath' => Yii::getPathOfAlias('appRoot'),
    'name' => 'novosel',
    'preload' => ['log', 'bootstrap'],
    'language' => 'ru',
    'import' => [
        'appRoot.models.*',
        'appRoot.components.*',
        'appRoot.modules.clients.models.*',
    ],
    'components' => [
        'bootstrap' => ['class' => 'bootstrap.components.Bootstrap'],
        'log' => $env['log'],
        'errorHandler' => ['errorAction' => 'site/error'],
        'db' => $env['db'],
        'urlManager' => [
            'urlFormat' => 'path', 'showScriptName' => false, 'caseSensitive' => false,
            'rules' => [
                # Заказы
                '/' => '/order/index/',
                '/order/update/id/<id:\d+>' => '/order/index/',
                '/order/_form_goods/' => '/order/formGoods/',
            ]
        ],
    ],
];
