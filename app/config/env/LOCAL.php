<?php

return [
    'db' => [
        'connectionString' => 'mysql:host=localhost;dbname=novosel',
        'username' => 'root',
        'password' => '',
        'emulatePrepare' => true,
        'charset' => 'utf8',
        'schemaCachingDuration' => 86400,
        'enableProfiling' => true,
        'enableParamLogging' => true,
    ],
    'log' => [
        'class' => 'CLogRouter',
        'routes' => [
            [
                'class' => 'vendors.yii1.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
                'ipFilters' => ['127.0.0.1']
            ],
        ],
    ],
];
