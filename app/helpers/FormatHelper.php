<?php

/** Хелпер формата строк */
class FormatHelper
{

  /** Приводит телефонный номер к удобочитаемому формату */
  public static function phone($phone)
  {
    $phoneLen = strlen($phone);
    if (10 == $phoneLen) {
      return '+7 (' . $phone[0] . $phone[1] . $phone[2] . ') ' . $phone[3] . $phone[4] . '-' . $phone[5] . $phone[6] . '-' . $phone[7] . $phone[8] . $phone[9];
    }
    return $phone;
  }

}
