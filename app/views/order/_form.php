<?php
/* Форма заказа */

/* @var $this OrderController */
/* @var $form TbActiveForm  */
/* @var $order Orders */


Yii::import('bootstrap.widgets.TbSelect2');
(new TbSelect2())->registerClientScript('');
?>

<? /** список всех доступные товаров с прайсом для связки с JS */ ?>
<?php $goods = Goods::model()->findAll(['select' => 'id, name, cost', 'index' => 'id']); ?>
<script id="js-list-of-available-goods" type="application/json"><?= CJSON::encode($goods) ?></script>


<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'type' => 'inline',
    'enableAjaxValidation' => false,
        ]);
?>

<h3>Добавить новый заказ</h3>
<fieldset>
  <div class="row">
<? /** данные заказчика */ ?>
    <div class="span4">
      <legend> данные заказчика </legend>
<?php echo $form->textFieldRow($order, 'customer_surname'); ?>
      <?php echo $form->textFieldRow($order, 'customer_name'); ?>
      <?php echo $form->textFieldRow($order, 'customer_phone'); ?>
      <?php echo $form->textFieldRow($order, 'customer_address'); ?>
    </div>

<? /** товары заказа */ ?>
    <div class="span8">
      <legend>товары</legend>

<?php echo $form->error($order, 'goods'); ?>

      <div class="js-order-form-goods-lines">
<?php foreach ($order->goods as $i => $goods): ?>
          <? $this->renderPartial('_form_goods', ['model' => $goods, 'i' => $i, 'form' => $form]) ?>
        <?php endforeach; ?>
      </div>

      <hr>
<? /** добавить новый товар */ ?>
      <div class="pull-left">
        <button type="button" class="btn btn-primary btn-xs" id="js-order-form-goods-add">+ добавить товар</button>
      </div>

<? /** итого */ ?>
      <div class="pull-right">
        Итого:  <span class="js-order-form-goods-total-cost"><?= $order->orderTotalCost; ?></span> р.
      </div>

    </div>
  </div>
</fieldset>


<? /* --- Добавить / Сохранить --- */ ?>
<div class="form-actions text-right">
<?php
$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'submit', 'type' => 'primary', 'label' => $order->isNewRecord ? 'Добавить заказ' : 'Обновить заказ'
]);
?>
</div>
  <?php $this->endWidget(); ?>


