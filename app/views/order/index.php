<?php

/* Orders dashboard */

/* @var $this OrderController */
/* @var $form TbActiveForm  */
/* @var $order Orders */
/* @var $dataProvider Orders */

$goodsList = Goods::model()->findAll();
$orderGoods = new OrderGoods();

$this->pageTitle = 'Заказы';
?>

<? /** форма */ ?>
<? $this->renderPartial('_form', ['order' => $order]) ?>


<? /** список заказов */ ?>
<? $this->renderPartial('_list', ['dataProvider' => $dataProvider, 'searchFilter' => $searchFilter]) ?>
