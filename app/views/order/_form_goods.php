<?php
/* Товары конкретного заказа */

/* @var $this OrderController */
/* @var $form TbActiveForm  */
/* @var $model OrderGoods */

Yii::import('bootstrap.widgets.TbActiveForm');
$form = new TbActiveForm();
$form->type = 'inline';

if (!isset($i)) $i = uniqid();
if (!isset($goods)) $goods = Goods::model()->findAll(['index' => 'id']);

if ($model->isNewRecord) {
  $goodsCost = 0;
  $goodsCount = 0;
} else {
  $goodsCost = $goods[$model->goods_id]->cost;
  $goodsCount = $model->count;
}
?>

<div class="js-order-form-goods-line order-form-goods-line">
  <? /** товар */ ?>
  <?php
  echo $form->select2Row($model, "[$i]goods_id", [
      'data' => ['0' => 'Выберете товар'] + CHtml::listData($goods, 'id', 'name'),
      'events' => ['change' => 'js: function(e) {orders.goods_changed(e)}']
          ]
  );
  ?>

  <? /** кол-во единиц */ ?>
  <?php echo $form->numberFieldRow($model, "[$i]count", ['placeholder' => 'кол-во единиц', 'style' => 'width:5em']); ?>

  <? /** стоимость единицы */ ?>
  <span> 
    x 
    <span class="js-order-form-goods-line-cost" 
          data-cost="<?= $goodsCost ?>" data-count="<?= $goodsCount ?>"><?= $goodsCost ?></span>
    р.
  </span>

</div>