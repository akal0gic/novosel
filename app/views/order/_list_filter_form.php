<?php
/* Форма-фильтр спика заказов */

/* @var $this OrderController */
/* @var $form TbActiveForm  */
/* @var $dataProvider Orders */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'action' => '/',
    'method' => 'get',
    'type' => 'inline',
    'enableAjaxValidation' => false,
        ]);
?>

<span> Дата создания заказа: от </span><?php
$this->widget('bootstrap.widgets.TbDatePicker', [
    'name' => 'OrdersSearch[date_from]',
    'value' => $searchFilter->date_from
]);
?>
<span>&nbsp; до </span><?php
$this->widget('bootstrap.widgets.TbDatePicker', [
    'name' => 'OrdersSearch[date_to]',
    'value' => $searchFilter->date_to
]);
?>
<span>&nbsp; тел. </span>
<?php echo $form->textFieldRow($searchFilter, 'customer_phone', ['name' => 'OrdersSearch[customer_phone]']); ?>

&nbsp;
<label> 
  <input type="checkbox" name="OrdersSearch[archived]" value="1"
         <?= $searchFilter->archived ? 'checked="checked"' : '' ?>/> <span>Показывать архивные заказы</span>
</label >

<hr>
<div class="text-right">  
  <?php
  $this->widget('bootstrap.widgets.TbButton', ['buttonType' => 'submit', 'type' => 'primary', 'label' => 'фильтровать']);
  ?>
</div>

<?php $this->endWidget(); ?>

