<?php
/* Список заказов */

/* @var $this OrderController */
/* @var $form TbActiveForm  */
/* @var $dataProvider Orders */

Yii::import('appRoot.helpers.FormatHelper');
?>

<h3>Список заказов</h3>
<? /** фильтр поиска */ ?>
<?php $this->renderPartial('_list_filter_form', ['searchFilter' => $searchFilter]) ?>

<? /** список */ ?>
<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    #
    'rowCssClassExpression' => '$data->status == "ARCHIVE" ? "archive" : ""',
    #
    'columns' => [
        'created_at' => [
            'name' => 'created_at',
            'value' => 'app()->dateFormatter->format("dd.MM.y H:m", strtotime($data->created_at))'
        ],
        'modified_at' => [
            'name' => 'modified_at',
            'value' => 'app()->dateFormatter->format("dd.MM.y H:m", strtotime($data->modified_at))'
        ],
        'customer_phone' => [
            'name' => 'customer_phone',
            'value' => 'FormatHelper::phone($data->customer_phone)',
        ],
        'customer_name' => [
            'name' => 'customer_name',
            'value' => '$data->customer_surname . " " . $data->customer_name'
        ],
        'customer_address',
        'cost',
        # кнопочки
        [
            'class' => 'CButtonColumn',
            'template' => '{update} &nbsp; {to_archive}{from_archive}',
            'buttons' => [
                'update' => [
                    'label' => 'редактировать',
                    'url' => '"/order/update/id/{$data->id}"',
                    'visible' => '"ARCHIVE" != $data->status'
                ],
                'to_archive' => [
                    'label' => 'в архив',
                    'url' => '"/order/archive_toggle/id/{$data->id}"',
                    'visible' => '"ARCHIVE" != $data->status'
                ],
                'from_archive' => [
                    'label' => 'востановить из архива',
                    'url' => '"/order/archive_toggle/id/{$data->id}"',
                    'visible' => '"ARCHIVE" == $data->status'
                ],
            ]
        ]
    ]
]);
?>