<?
/** Основной layout */
/* @var $this SiteController */
?><!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?= $this->pageTitle ?></title>

    <link href="/css/bootstrap.min.css" rel="stylesheet" media="all" />
    <link href="/css/main.css" rel="stylesheet" media="all" />
    <script src="/js/main.js"></script>
  </head>
  <body>

    <div class="container">
      <br><br>

      <?= $content; ?>

    </div>
  </body>
</html>