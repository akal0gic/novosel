<?php


# Ярлыки для компонентов и основных свойств фреймворка  ----------------------------------------------------------------------------------------------

/** This is the shortcut to app()
 * @return CWebApplication */
function app() {
    return Yii::app();
}

/** This is the shortcut to app()->cache
 * @return CCache */
function cache() {
    return app()->cache;
}

/** Returns the named application parameter.
 * This is the shortcut to app()->params[$name]. */
function param($name, $default = NULL) {
	return isset(app()->params[$name]) ? app()->params[$name] : $default;
}

/** This is the shortcut to app()->user.
 * @return WebUser */
function user() {
    return app()->user;
}

/** This is the shortcut to app()->clientScript
 * @return CClientScript */
function cs() {
    return app()->clientScript;
}

/** This is the shortcut to app()->db
 * @return CDbConnection */
function db() {
    return app()->db;
}

/** This is the shortcut to app()->db_users
 * @return CDbConnection */
function usersDB() {
    return app()->db_users;
}

/** This is the shortcut to app()->request
 * @return CHttpRequest object */
function request() {
    return app()->request;
}

/** Shortcut to app()->format (utilities for formatting structured text) */
function format() {
    return app()->format;
}

/** @return Cookie - Shortcut to app()->cookie */
function cookie() {
    return app()->cookie;
}

/** @return Payments - Shortcut to app()->payments */
function payments() {
    return app()->payments;
}

/** @return CHttpSession - Shortcut to app()->session */
function session() {
    return app()->session;
}

/** This is the shortcut to app()->user->checkAccess(). */
function allow($operation, $params = [], $allowCaching = true) {
    return app()->user->checkAccess($operation, $params, $allowCaching);
}


/** Displays a variable.
 * This method achieves the similar functionality as var_dump and print_r
 * but is more robust when handling complex objects such as Yii controllers.
 * @param mixed variable to be dumped
 * @param integer maximum depth that the dumper should go into the variable. Defaults to 10.
 * @param boolean whether the result should be syntax-highlighted */
function dump($target, $doEnd = FALSE, $depth = 10, $highlight = true) {
    echo CVarDumper::dumpAsString($target, $depth, $highlight);
    if ($doEnd) {
        app()->end();
    }
}