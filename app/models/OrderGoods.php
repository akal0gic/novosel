<?php

/**
 *  Товары привязанные к заказу
 */
class OrderGoods extends ActiveRecord
{

  /** @var int                  -  Primary Key */
  public $id;

  /** @var int                  -  pk Заказа */
  public $order_id;

  /** @var int                  -  pk Товара */
  public $goods_id;

  /** @var int                  -  кол-во товаров этого типа в заказе */
  public $count;

  public function tableName()
  {
    return 'order_goods';
  }

  /**
   * @return OrderGoods 
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  public function relations()
  {
    return [
        'goods' => [
            self::BELONGS_TO, 'Goods', 'goods_id',
        ],
    ];
  }

  public function rules()
  {
    return [
        ['count, goods_id', 'required'],
        ['count', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 99],
        ['goods_id', 'exist', 'allowEmpty' => false, 'attributeName' => 'id', 'className' => 'Goods', 'message' => 'Выберете товар']
    ];
  }

  public function attributeLabels()
  {
    return [
        'count' => 'кол-во',
    ];
  }

}
