<?php

/**
 * Товары
 */
class Goods extends ActiveRecord
{

  public function tableName()
  {
    return 'goods';
  }

  /**
   * @return Goods 
   */
  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

}
