<?php

/**
 * Заказы
 * 
 * @property OrderGoods $goods  -  Товары в рамках этого заказа
 */
class Orders extends ActiveRecord
{

  /** @var int                    - Primary Key */
  public $id;

  /** @var string|datetime        - Время создания заказа */
  public $created_at;

  /** @var string|datetime       -  Время последнего изменения заказа */
  public $modified_at;

  /** @var int                   -  Региональный тел. номер заказчика */
  public $customer_phone;

  /** @var string                -  Адрес заказчика */
  public $customer_address;

  /** @var string                -  Фамилия заказчика */
  public $customer_surname;

  /** @var string                -  Имя заказчика */
  public $customer_name;

  /** @var mixed                 -  Стаус заказа */
  public $status;

  /** @var int                   -  Стоимость заказа */
  public $cost;
  public $goods = [];

  # атрибуты поиска
  public $date_from = null;
  public $date_to = '';
  public $archived = false;

  /** */
  public function tableName()
  {
    return 'orders';
  }

  /**
   * @return Orders 
   */
  public static function model($className = __CLASS__)
  {
    return parent::model('orders');
  }

  public function relations()
  {
    return array(
        'goods_relation' => [
            self::MANY_MANY, 'OrderGoods', 'order_goods(order_id, id)'
        ],
        'orderTotalCost' => [self::STAT, 'Goods', 'order_goods(order_id, goods_id)', 'select' => 'SUM(cost * count)']
    );
  }

  public function rules()
  {
    return $this->filterTrim() +
            $this->defaultRulesAndFiltersForPhone('customer_phone') +
            [
                ['customer_phone, customer_address, customer_surname, customer_name', 'required'],
                ['status', 'in', 'range' => ['NEW', 'ARCHIVE']],
                # search
                ['customer_phone, archived', 'numerical', 'on' => 'search'],
                ['date_from, date_to', 'safe', 'on' => 'search'],
    ];
  }

  /** @return array customized attribute labels (name=>label) */
  public function attributeLabels()
  {
    return [
        # атрибуты
        'created_at' => 'Создан',
        'modified_at' => 'Изменен',
        'customer_surname' => 'Фамилия',
        'customer_name' => 'Имя',
        'customer_phone' => 'Телефон',
        'customer_address' => 'Адрес',
        'cost' => 'Общая стоимость заказа',
        # relations
        'orderTotalCost' => 'Общая стоимость заказа',
    ];
  }

  protected function beforeSave()
  {
    # время создания и измения модели
    if ($this->isNewRecord) $this->created_at = new CDbExpression('NOW()');
    $this->modified_at = new CDbExpression('NOW()');

    # пересчитать полную стоимость заказа
    $this->cost = 0;
    foreach ($this->goods as $set) {
      $this->cost += $set->count * $set->goods->cost;
    }

    return parent::beforeSave();
  }

  protected function afterSave()
  {
    # обновить связаные модели (товары)
    OrderGoods::model()->deleteAll('order_id = ' . $this->id);
    foreach ($this->goods as $goods) {
      $goods->order_id = $this->id;
      $goods->save();
    }

    parent::afterSave();
  }

  public function search(Orders $searchFilter = null)
  {
    $criteria = new CDbCriteria;
    if ($searchFilter) {
      $criteria->compare('customer_phone', $searchFilter->customer_phone, true);
      if ($searchFilter->date_from)
          $criteria->addCondition('created_at >= "' . date('Y.m.d', strtotime($searchFilter->date_from)) . '"');
      if ($searchFilter->date_to)
          $criteria->addCondition('created_at <= "' . date('Y.m.d', strtotime($searchFilter->date_to)) . '"');
      $criteria->addCondition('status = "' . ($searchFilter->archived ? 'ARCHIVE' : 'NEW') . '"');
    }

    return new CActiveDataProvider('Orders', [
        'criteria' => $criteria,
        'sort' => ['defaultOrder' => 't.modified_at DESC'],
        'pagination' => ['pageSize' => 20],
    ]);
  }

}
