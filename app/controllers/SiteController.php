<?php

/** SiteController.php */
class SiteController extends Controller
{

  /** Default error controller */
  public function actionError()
  {
    if (($error = app()->errorHandler->error)) {
      if (app()->request->isAjaxRequest) echo $error['message'];
      else $this->render('error/error', $error);
    }
  }

}
