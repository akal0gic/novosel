<?php

/**
 * OrderController.php 
 * Управление закзазами
 * 
 * @method Orders initModel(null|int $id) Description
 */
class OrderController extends Controller
{

  public $modelName = 'Orders';

  /** Главная страница */
  public function actionIndex($id = null)
  {
    $order = $this->initModel($id);
    $order->goods = $order->goods_relation;

    # валидация и сохранение
    $this->validateAndSave($order);

    # расширенный поиск
    $searchFilter = new Orders('search');
    $searchFilter->setAttributes(request()->getParam('OrdersSearch'));

    $this->render('index', [
        'order' => $order,
        'dataProvider' => Orders::model()->search($searchFilter),
        'searchFilter' => $searchFilter,
    ]);
  }

  /** Переносит заказы в архив и обратно */
  public function actionArchive_toggle($id = null)
  {
    $order = $this->initModel($id);

    $order->status = ('ARCHIVE' == $order->status) ? 'NEW' : 'ARCHIVE';
    $order->save(false);

    $this->redirect('/');
  }

  /** подгружает новые товары к заказу */
  public function actionFormGoods()
  {
    if (!request()->isAjaxRequest) throw new CHttpException(500, 'not allowed');
    $this->renderPartial('_form_goods', ['model' => new OrderGoods()]);
  }

  /** валидация и сохранение заказа */
  public function validateAndSave(Orders $order)
  {
    if (!($attributes = request()->getPost('Orders'))) return false;
    $order->setAttributes($attributes);

    # валидация всех товаров добавленых к заказу
    $order->goods = [];
    $is_all_goods_valid = true;
    $valid_goods_count = 0;

    if (($attributes = request()->getPost('OrderGoods'))) {


      foreach ($attributes as $i => $goods_set) {
        $goods = new OrderGoods();
        $goods->setAttributes($goods_set, false);
        $order->goods[$i] = $goods;

        if (!$goods->validate()) {
          $is_all_goods_valid = false;
          continue;
        } else $valid_goods_count++;
      }
    }

    if ($valid_goods_count < 1 || !$is_all_goods_valid) $order->addError('goods', 'Укажите хотя бы один товар');
    if (!$order->hasErrors() && $order->validate(null, false)) {
      $order->save(true);
      $this->redirect('/');
    }
  }

}
